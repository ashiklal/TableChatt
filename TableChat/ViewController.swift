//
//  ViewController.swift
//  TableChat
//
//  Created by Admin on 24/05/22.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var chatTV: UITableView!
    var sarray = ["hi ","hello","how are u","wer r u from","ya am gud","okay","c u ","tata","ya","by"]
    @IBOutlet weak var senderTF:UITextField!
    var rarray = [1,2,1,2,1,2,1,2,1,2]
    
    @IBOutlet weak var msgBox: GrowingTextView!
    @IBOutlet weak var SendBTN: UIButton!
    
    @IBAction func SendB(_ sender: UIButton) {
//        sarray.insert(msgBox.text, at: 0)
        var tvalue = 1
        
        if tvalue == 1{
            print(tvalue)
            tvalue = 1
        }
        else if tvalue == 2 {
            print(tvalue)
            tvalue = 1
        }
//        rarray.insert(tvalue, at: <#T##Int#>)
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let scrollPoint = CGPoint(x: 0, y: self.chatTV.contentSize.height - self.chatTV.frame.size.height)
        self.chatTV.setContentOffset(scrollPoint, animated: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        let scrollPoint = CGPoint(x: 0, y: self.chatTV.contentSize.height - self.chatTV.frame.size.height)
        self.chatTV.setContentOffset(scrollPoint, animated: false)
    }

}
extension ViewController:UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rcell = chatTV.dequeueReusableCell(withIdentifier: "receiverercell", for: indexPath) as! receiverercell
        let scell = chatTV.dequeueReusableCell(withIdentifier: "sendercell", for: indexPath) as! sendercell
        if rarray[indexPath.row] == 1{
            rcell.receivermessage.text = sarray[indexPath.row]
            rcell.rview.layer.cornerRadius = 15
            return rcell
        }
        else{
            scell.sendermessage.text = sarray[indexPath.row]
            scell.sview.layer.cornerRadius = 15
            return scell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}














//class//
class sendercell:UITableViewCell{
    @IBOutlet weak var sendermessage:UILabel!
    @IBOutlet weak var sview:UIView!
}

class receiverercell:UITableViewCell{
    @IBOutlet weak var receivermessage:UILabel!
    @IBOutlet weak var rview:UIView!
}
extension UITableView {
    func scrollToBottomRow() {
        DispatchQueue.main.async { [weak self] in
        guard let this = self else { return }
            guard this.numberOfSections > 0 else { return }
            
            // Make an attempt to use the bottom-most section with at least one row
            var section = max(this.numberOfSections - 1, 0)
            var row = max(this.numberOfRows(inSection: section) - 1, 0)
            var indexPath = IndexPath(row: row, section: section)
            
            // Ensure the index path is valid, otherwise use the section above (sections can
            // contain 0 rows which leads to an invalid index path)
            while !this.indexPathIsValid(indexPath) {
                section = max(section - 1, 0)
                row = max(this.numberOfRows(inSection: section) - 1, 0)
                indexPath = IndexPath(row: row, section: section)
                
                // If we're down to the last section, attempt to use the first row
                if indexPath.section == 0 {
                    indexPath = IndexPath(row: 0, section: 0)
                    break
                }
            }
            
            // In the case that [0, 0] is valid (perhaps no data source?), ensure we don't encounter an
            // exception here
            guard this.indexPathIsValid(indexPath) else { return }
            this.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func indexPathIsValid(_ indexPath: IndexPath) -> Bool {
        let section = indexPath.section
        let row = indexPath.row
        return section < self.numberOfSections && row < self.numberOfRows(inSection: section)
    }
}

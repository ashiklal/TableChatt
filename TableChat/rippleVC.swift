//
//  rippleVC.swift
//  TableChat
//
//  Created by Admin on 25/05/22.
//

import UIKit


class rippleVC: UIViewController {
    @IBOutlet weak var rview:UIView!
    @IBOutlet weak var bview:UIView!
    @IBOutlet weak var BviewHeight: NSLayoutConstraint!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BviewHeight.constant = 0
        bview.layer.cornerRadius = 20
        bview.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        rview.layer.cornerRadius = (rview.frame.height) / 2
        addRippleEffect(to: rview)
      
    }
    
    @IBAction func Btn(_ sender: UIButton) {
        if BviewHeight.constant == 0{
            setView(view: bview)
        }
        else {
            BviewHeight.constant = 0
        }
        
    }
    
    
    
    
    
    
    func setView(view: UIView) {
        UIView.transition(with: view, duration: 1.0, options: .curveLinear , animations: {
                self.BviewHeight.constant = 300
            })
        }

    
    
    
    func addRippleEffect(to referenceView: UIView) {
           /*! Creates a circular path around the view*/
           let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height))
           /*! Position where the shape layer should be */
           let shapePosition = CGPoint(x: referenceView.bounds.size.width / 2.0, y: referenceView.bounds.size.height / 2.0)
           let rippleShape = CAShapeLayer()
           rippleShape.bounds = CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height)
           rippleShape.path = path.cgPath
           rippleShape.fillColor = UIColor.clear.cgColor
           rippleShape.strokeColor = UIColor.red.cgColor
           rippleShape.lineWidth = 4
           rippleShape.position = shapePosition
           rippleShape.opacity = 0
           
           /*! Add the ripple layer as the sublayer of the reference view */
           referenceView.layer.addSublayer(rippleShape)
           /*! Create scale animation of the ripples */
           let scaleAnim = CABasicAnimation(keyPath: "transform.scale")
           scaleAnim.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        scaleAnim.toValue = NSValue(caTransform3D: CATransform3DMakeScale(1.2, 1, 1))
           /*! Create animation for opacity of the ripples */
           let opacityAnim = CABasicAnimation(keyPath: "opacity")
           opacityAnim.fromValue = 1
           opacityAnim.toValue = nil
           /*! Group the opacity and scale animations */
           let animation = CAAnimationGroup()
           animation.animations = [scaleAnim, opacityAnim]
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
           animation.duration = CFTimeInterval(0.7)
        animation.repeatCount = .infinity
           animation.isRemovedOnCompletion = true
           rippleShape.add(animation, forKey: "rippleEffect")
       }
    

}
